import random
import time
import operator
from operator import attrgetter
from datetime import timedelta

maxScore = maxError = 10
score = error = 0
quesNum = 1

# Main class
class User(object):
    """
    Stores data (username, time, and error) in this class.
    """
    def __init__(self, username = "", time = "", error = 0):
        self.username = username
        self.time = time
        self.error = error  

# Reads usernames in a text file.
def readUser(file):
    userList = []
    userFile = open(file, "r")

    for x in userFile:
        data = x.strip("\n").split("\t")
        user = User(data[0], data[1], data[2])
        userList.append(user)

    userFile.close()
    return userList

# Stores data into file.
def setUser(file, userList):
    thisFile = open(file, 'w')
    
    for x in xrange(len(userList)):
        thisFile.writelines(userList[x].username + '\t' + userList[x].time + '\t' + str(userList[x].error) + '\n')

    thisFile.close()

# Gets operator, and two random numbers.
def getNum(op):
    if op == "+":
        num1 = random.randint(10,99)
        num2 = random.randint(10,99)

    elif op == "-":
        num1 = random.randint(10,99)
        num2 = random.randint(10,99)
        while num1 < num2:
            num2 = random.randint(10,99)

    elif op == "*":
        num1 = random.randint(10,99)
        num2 = random.randint(2,9)
    
    elif op == "/":
        num1 = random.randint(100,999)
        num2 = random.randint(2,9)
        while num1%num2 != 0:
            num1 = random.randint(100,999)

    return [num1, op, num2]  

# Checks if list is updated.
def isUpdated(username, currTime, error):
    global userList

    for x in xrange(len(userList)):
        if username == userList[x].username:
            prevTime = userList[x].time
            if prevTime > currTime or (error < userList[x].error and prevTime == currTime):
                userList[x].username = username
                userList[x].time = time
                userList[x].error = error
                
            return True
    return False

# Sets score for the username.
def setScore(username, endTime, score):
    global userList
    global isUpdated 
    ifUpdate = isUpdated(username, endTime, score)
    if not ifUpdate:
        thisUser = User(username, endTime, error)
        userList.append(thisUser)

# Ends the timer.
def getTime(startTime):
    endTime = int(time.time())-int(startTime)
    return timedelta(seconds = endTime)

# Sorts and displays top 10 users by the fastest time.
def sortUserList(userList):
    sortedList = sorted(userList, key = attrgetter("time", "error"))
    
    for x in xrange(len(sortedList)):
        if x is 10:
            break
        
        finalUsername = sortedList[x].username
        finalTime = sortedList[x].time
        finalError = str(sortedList[x].error)
        print "\nTop 10 Users Based On Time:"
        print str(x + 1) + ".) " + finalUsername + " (Time: " + finalTime + " Error/s: " + finalError + ")"
    
    return sortedList

# Operators.
ans = { "+":operator.add,
        "-":operator.sub,
        "*":operator.mul,
        "/":operator.truediv }

userList = readUser("user.txt")                             # Puts data into a list.
username = raw_input("Enter username (case sensitive): ")   # User inputs username.

# Game loops until [enter] is pressed while asking for username
while username:
    print "\nWelcome, {}!".format(username)
    start = raw_input("Press [enter] to begin.")
    
    # Timer starts
    print "\nTime starts now! Please answer the following:"
    startTime = time.time()

    # Game starts
    while score < maxScore and error < maxError:
        oper = random.choice(ans.keys())                    # Randomize the operations.
        num = getNum(oper)                                  # Uses operation for the two random numbers.
        answer = ans.get(oper)(num[0],num[2])               # Gets the answer.

        while 1:
            try:
                print "Question number {}:".format(quesNum)   
                question = map(str,num)   
                playerans = int(raw_input(' '.join(question) + " = "))
                quesNum += 1
            except ValueError:
                print "Input is not an integer. Try again!\n"
            else:
                break

        if int(playerans) == answer:
            score += 1
            print "Your answer, {}, is correct!".format(answer)
        else:
            error += 1
            print "Your answer, {}, is wrong!".format(answer)
        
    # Score reaches 10, displays congratulations
    if score == maxScore:
        endTime = str(getTime(startTime))

        print "Congratulations, {}!\nScore: {}. Error: {}. Time: {}.\n".format(username, score, error, endTime)
        setScore(username, endTime, score)
        
    # Error reaches 10
    if error == maxError:
        print "Game Over, {}!\nScore: {}. Error: {}.\n\n".format(username, score, error)
        score = error = 0

    # Asks username for another game
    username = raw_input("Enter username (case sensitive): ")

finalSortedList = sortUserList(userList)
setUser("user.text", finalSortedList)
